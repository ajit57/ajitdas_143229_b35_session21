<?php
//print_r(PDO::getAvailableDrivers());
namespace App\Modal;
use PDO;
class Database
{
    public $DBH;
    public $host="localhost";
    public $dbname = "atomic_project_b35";
    public $user = "root";
    public $pass = "";
    public function __construct()
    {
        try {
            # MySQL with PDO_MYSQL
            $DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->pass);
            echo "Database Connected Successfully...........";
        }
        catch(\PDOException $e){
            echo "I'm sorry, Dave. I'm afraid I can't do that.";
            $DHBerrors = $e->getMessage()."/n";
            file_put_contents('PDOErrors.txt', $DHBerrors, FILE_APPEND);
        }
    }
}
